import numpy as np
from scipy.stats import norm, gamma
import seaborn as sns
import matplotlib.pyplot as plt
from tqdm import tqdm
from plots import *
from gelman_rubin import *
from scipy import optimize
from joblib import Parallel, delayed

def loglike(param):
    return sum(norm.logpdf(y, loc=param[0]+x*param[1], scale=param[2]))

def logprior(param):
    p0 = norm.logpdf(param[0], loc=0, scale=10)
    p1 = norm.logpdf(param[1], loc=1, scale=10)
    p2 = gamma.logpdf(param[2], a=2)
    return p0+p1+p2

def logpost(param):
    return (logprior(param)+loglike(param))

def metropolis(param0, niter, logpost, accept_target=0.5, cov=None):
    
    """ Description: Run a random walk metropolis-hasting for a given log posterior
    param(i) ~ N[param(i-1), scale * cov]
    
    Inputs:
    --------
    param0:        (array)    Initial parameters of metropolis
    niter:         (int)      Number of iterations
    logpost:       (function) 
    accept_target: (float)    Target acceptance rate
    """
    global acceptance_rate
    if cov is None:
        cov = np.eye(len(param0))

    scale = 2.38/pow(len(param0), 0.5) # a rule-of-thumb initial scale
    
    accept = 0
    samples = np.zeros((1+niter, len(param0)))
    samples[0,:] = param0

    progress_bar = tqdm(range(1,niter+1))
    for i in progress_bar:
        proposal = np.random.multivariate_normal(samples[i-1,:], scale*cov) # sample a proposal
        if (np.log(np.random.uniform(0,1)) < (logpost(proposal)-logpost(samples[i-1,:]))): # metropolis-step
            samples[i,:] = proposal
            accept += 1
        else:
            samples[i,:] = samples[i-1,:]
        acceptance_rate=accept/i
        # update the scale parameter to achieve the target acceptance rate
        if i%100==0 and i<niter*0.5:
            New_scale = scale*(norm.ppf(accept_target/2)/norm.ppf(acceptance_rate/2))
            if New_scale>0:
                scale=New_scale
            else:
                scale=0.001
        progress_bar.set_postfix({'acceptance rate': acceptance_rate})
    return samples[1:]

# Parallelizing
def run_chains(number_of_chains):
    param0 = np.random.normal(size=3, loc=0, scale=10**.5) # initiate at different initial values
    param0[2] = param0[2]**2
    output=metropolis(param0=param0, niter=100000, logpost=logpost, accept_target=0.5, cov=None)
    print("Acceptance rate for chain {}".format(number_of_chains), acceptance_rate)
    return output

if __name__ == "__main__":
    #================================
    # Simulate data
    #================================
    N = 1000
    u = np.random.normal(size=N, loc=0, scale=2)
    x = np.random.normal(size=N)
    y = 3+x*0.5 + u

    fig, ax = plt.subplots(figsize=stdfigsize(nx=2, ny=1.5, scale=1))
    ax.scatter(x,y)
    ax.set_xlabel(r"$x$")
    ax.set_ylabel(r"$y$")
    fig.tight_layout()
    plt.show()

    loglike([3,0.5, 2])

    #================================
    # OLS
    #================================

    X = np.vstack((np.ones(len(x)),x)).T

    param_ols = np.linalg.solve(X.T@X, X.T@y)
    uhat = y - X@np.linalg.solve(X.T@X, X.T@y)

    param_ols = np.append(param_ols, np.sqrt((1/(N-2))*uhat.T@uhat))
    print("OLS estimates": param_ols)


    #================================
    # Maximum likelihood
    #================================

    max_like = optimize.minimize(lambda param: -loglike(param), x0=param_ols, method='BFGS')
    param_ml = max_like.x
    print("ML estimates": param_ml)

    #================================
    # Bayesian estimation
    #================================

    chains = Parallel(n_jobs=3, backend="loky")(delayed(run_chains)(i) for i in range(3))
    # TODO: show the progress bar of each chain (is it possible?)


    samples1 = chains[0][50000:]
    samples2 = chains[1][50000:]
    samples3 = chains[2][50000:]


    nburn = 0
    coef = ["$\\alpha$", "$\\beta$", "$\\sigma$"]

    fig, ax = plt.subplots(nrows=3, ncols=2, figsize=stdfigsize(nx=2, ny=2, scale=1.2))

    for i in range(3):
        for j in range(2):
            
            
            
            if j==0:
                ax[i,j].plot(samples1[nburn:, i], alpha=0.3)
                ax[i,j].plot(samples2[nburn:, i], alpha=0.3)
                ax[i,j].plot(samples3[nburn:, i], alpha=0.3)
                ax[i,j].set_xlabel("Iterations")
                ax[i,j].set_ylabel(coef[i])
                
                
            else:
                sns.kdeplot(samples1[nburn:, i], ax=ax[i,j], alpha=0.3)
                sns.kdeplot(samples2[nburn:, i], ax=ax[i,j], alpha=0.3)
                sns.kdeplot(samples3[nburn:, i], ax=ax[i,j], alpha= 0.3)
                
                ax[i,j].axvline(np.mean(samples1[nburn:, i]), linestyle="--", alpha=0.3)
                ax[i,j].axvline(np.mean(samples2[nburn:, i]), linestyle="--", alpha=0.3)
                ax[i,j].axvline(np.mean(samples3[nburn:, i]), linestyle="--", alpha=0.3)
                
                ax[i,j].set_xlabel(coef[i])
                ax[i,j].set_ylabel("Density")
    fig.tight_layout()
    plt.show()

    print("Gelman-Rubin Rhat:\n",gelman_rubin([samples1[nburn:],
                        samples2[nburn:],
                        samples3[nburn:]]))


