import plotly.graph_objects as go
import pandas as pd

def ts_plot(df, title='Untitled'):
    """
    Create a time series plot using Plotly.

    Parameters:
        title (str): Title of the plot.
        df (DataFrame): Pandas DataFrame containing the time series data.
    """
    # Magma
    magma_colors = [
        "#000004", "#120B3D", "#3B0F70", "#6D147C",
        "#9F179E", "#D728AE", "#F36B6E", "#FCB63A"
    ]
    
    # Plasma
    plasma_colors = [
        "#0D0887", "#46039F", "#7201A8", "#9C179E",
        "#BD3786", "#D8576B", "#ED7953", "#FB9F3A"
    ]
    
    # Turbo
    turbo_colors = [
        "#23171F", "#2F1E31", "#3D2C43", "#4B3A54",
        "#585865", "#666576", "#737387", "#808197"
    ]
    
    # Cividis
    cividis_colors = [
        "#00204C", "#193461", "#405A6C", "#6D7446",
        "#A58C29", "#D3A000", "#F0C600", "#FCF300"
    ]
    
    colors = cividis_colors
    colors = ["#205375", "#DB6B97", "#940303", "#5eacff"]


    count_ytd = len(df.loc[str(df.index.year[-1]):].resample('MS').mean())
    # Define layout with figure size
    layout = go.Layout(
        width=1100,  # Width of the figure in pixels
        height=700,  # Height of the figure in pixels
        xaxis=dict(
            rangeselector=dict(
                buttons=list([
                    # dict(count=1, label="1d", step="day", stepmode="backward"),
                    dict(count=1, label="1m", step="month", stepmode="backward"),
                    dict(count=3, label="3m", step="month", stepmode="backward"),
                    dict(count=6, label="6m", step="month", stepmode="backward"),
                    dict(count=1, label="1y", step="year", stepmode="backward"),
                    dict(count=5, label="5y", step="year", stepmode="backward"),
                    dict(count=count_ytd, label="YTD", step="month", stepmode="backward"),
                    dict(step="all", label='All')
                ])
            ),
            rangeslider=dict(
                visible=True,
                thickness=0.1
            ),
            type="date"
        )
    )
    
    # Create a Plotly figure
    fig = go.Figure(layout=layout)

    # Add a time series trace
    for i, column in enumerate(df.columns):
        color_index = i * 1  # Change the multiplier to adjust the interval
        if color_index >= len(colors):
            color_index = color_index % len(colors)
        fig.add_trace(
            go.Scatter(
                x=df.index,
                y=df[column],
                mode='lines',
                name=column,
                line=dict(color=colors[color_index])  # Assign colors from the palette cyclically
            )
        )

    # Update layout
    fig.update_layout(
        title=title,
        xaxis_title='Date',
        yaxis_title='Value',
        template='simple_white',  # Use simple_white template
        yaxis=dict(
            gridcolor='lightgrey',  # Add gridlines for the y-axis
            gridwidth=1  # Set grid width
        )
    )

    fig.update_yaxes(
        showgrid=True, 
        gridwidth=0.1,
        gridcolor='rgba(211,211,211,0.6)',
        griddash='dot',
        minor_griddash=None,
        zeroline=False, 
        # zerolinewidth=2, 
        # zerolinecolor='black'
    )

    # fig.update_yaxes(
    #     zeroline=True,
    #     zerolinewidth=0.5, 
    #     zerolinecolor='black'
    # )

    # Show the plot
    fig.show()

# Example usage:
# Assuming 'df' is your DataFrame containing time series data
# You can call the function like this:
# ts_plot(df, title='Time Series Plot')

import plotly.graph_objects as go
import pandas as pd

def plot_contribution(contributions, fitted_values, title='Contribution Plot', marker=False):
    """
    Create a stacked bar chart for variable contributions with a fitted line using Plotly.

    Parameters:
        contributions (DataFrame): DataFrame where each column is a variable's contribution over time.
        fitted_values (Series): Series of fitted values from the regression model.
        title (str): Title of the plot.
        marker (bool): If True, includes markers on the fitted values line plot.
    """
    # Cividis
    cividis_colors = [
        "#00204C", "#193461", "#405A6C", "#6D7446",
        "#A58C29", "#D3A000", "#F0C600", "#FCF300"
    ]
    
    # Define color palette
    colors = ["#205375", "#DB6B97", "#940303", "#5eacff"]
    colors += cividis_colors

    # Define layout
    layout = go.Layout(
        width=1100,
        height=700,
        xaxis=dict(
            rangeselector=dict(
                buttons=list([
                    dict(count=1, label="1m", step="month", stepmode="backward"),
                    dict(count=3, label="3m", step="month", stepmode="backward"),
                    dict(count=6, label="6m", step="month", stepmode="backward"),
                    dict(count=1, label="1y", step="year", stepmode="backward"),
                    dict(count=5, label="5y", step="year", stepmode="backward"),
                    dict(step="all", label='All')
                ])
            ),
            rangeslider=dict(
                visible=True,
                thickness=0.025
            ),
            type="date"
        )
    )

    # Create a Plotly figure
    fig = go.Figure(layout=layout)

    # Add a line plot for fitted values with or without markers based on the parameter
    fig.add_trace(
        go.Scatter(
            x=fitted_values.index,
            y=fitted_values,
            mode='lines+markers' if marker else 'lines',
            name='Fitted Values',
            line=dict(color='rgba(3, 3, 3, 0.7)', width=2)
        )
    )

    # Add stacked bar chart for contributions
    for i, column in enumerate(contributions.columns):
        color_index = i % len(colors)  # Cycle colors if there are more columns than colors
        fig.add_trace(
            go.Bar(
                x=contributions.index,
                y=contributions[column],
                name=f'Contribution of {column}',
                marker_color=colors[color_index],
                offsetgroup=0
            )
        )

    # Update layout to stack bars relative to each other
    fig.update_layout(
        barmode='relative',  # Stacks bars so they sum to fitted values
        title=title,
        xaxis_title='Date',
        yaxis_title='Value',
        template='simple_white',
        yaxis=dict(
            gridcolor='lightgrey',
            gridwidth=1
        )
    )

    fig.update_yaxes(
        showgrid=True,
        gridwidth=0.1,
        gridcolor='rgba(211,211,211,0.6)',
        griddash='dot',
        zeroline=False
    )

    # Return the Plotly figure
    return fig

# Example usage:
# plot_contribution(contributions, fitted_values, title='Contribution Plot', marker=True)
