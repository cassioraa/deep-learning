import numpy as np
import pandas as pd
import scipy.sparse as sp
import matplotlib.pyplot as plt
from scipy.sparse import eye, diags
from scipy.sparse.linalg import spsolve

def l1tf(y, lambd):
    """
    Solve the l1 trend filtering problem.

    Parameters:
    y : numpy array
        Original signal (n-vector).
    lambd : float
        Positive regularization parameter.

    Returns:
    x : numpy array
        Primal optimal point (n-vector).
    z : numpy array
        Dual optimal point (2(n-1)-vector).
    status : str
        'solved' if solved, 'maxiter exceeded' if maximum iterations exceeded.
    """

    # Parameters
    ALPHA = 0.01   # backtracking linesearch parameter (0,0.5]
    BETA = 0.5     # backtracking linesearch parameter (0,1)
    MU = 2         # IPM parameter: t update
    MAXITER = 40   # IPM parameter: max iteration of IPM
    MAXLSITER = 20 # IPM parameter: max iteration of line search
    TOL = 1e-4     # IPM parameter: tolerance

    # Dimensions
    n = len(y)     # length of signal x
    m = n - 2      # length of Dx


    # Operator Matrices
    I2 = np.eye(n - 2, n - 2)
    O2 = np.zeros((n - 2, 1))
    D = np.c_[I2, O2, O2] +  np.c_[O2, -2*I2, O2] + np.c_[O2, O2, I2]

    DDT = D @ D.T
    Dy = D @ y

    # Variables
    z = np.zeros((m,1))          # dual variable
    mu1 = np.ones((m,1))         # dual of dual variable
    mu2 = np.ones((m,1))         # dual of dual variable

    t = 1e-10
    pobj = np.inf
    dobj = 0
    step = np.inf
    f1 = z - lambd
    f2 = -z - lambd

    print('--------------------------------------------')
    print('l1 trend filtering via primal-dual algorithm')
    print('--------------------------------------------')
    print('\n{:<10} {:<15} {:<15} {:<10}'.format('Iteration', 'Primal obj.', 'Dual obj.', 'Gap'))

    DTz = D.T @ z
    DDTz = D @ DTz
    w = Dy - (mu1 - mu2)
    
    pobj1 = 0.5 * (w.T @ np.linalg.solve(DDT, w)) + lambd * np.sum(mu1 + mu2)
    pobj2 = 0.5 * (DTz.T @ DTz) + lambd * np.sum(np.abs(Dy - DDTz))

    # Main loop
    for iters in range(MAXITER):
        DTz = D.T @ z
        DDTz = D @ DTz
        w = Dy - (mu1 - mu2)
        
        pobj1 = 0.5 * (w.T @ np.linalg.solve(DDT, w)) + lambd * np.sum(mu1 + mu2)
        pobj2 = 0.5 * (DTz.T @ DTz) + lambd * np.sum(np.abs(Dy - DDTz))
        pobj = min(pobj1, pobj2)
        dobj = -0.5 * (DTz.T @ DTz) + Dy.T @ z
        gap = pobj - dobj

        print('{:<10} {:<15.4e} {:<15.4e} {:<10.2e}'.format(iters, pobj[0,0], dobj[0,0], gap[0,0]))

        # Stopping criterion
        if gap <= TOL:
            status = 'solved'
            print(status)
            x = y - D.T @ z
            # return x, z, status

        if step >= 0.2:
            t = max(2 * m * MU / gap, 1.2 * t)

        # Calculate Newton step
        rz = DDTz - w
        S = DDT - diags((mu1 / f1 + mu2 / f2).reshape(-1))
        r = -DDTz + Dy + (1 / t) / f1 - (1 / t) / f2
        dz = spsolve(S, r)
        dz = dz.reshape((len(dz), 1))

        dmu1 = -(mu1 + ((1 / t) + dz * mu1) / f1)
        dmu2 = -(mu2 + ((1 / t) - dz * mu2) / f2)
        
        resDual = rz
        resCent = np.concatenate([-mu1 * f1 - 1 / t, -mu2 * f2 - 1 / t])
        residual = np.concatenate([resDual, resCent])

        # Backtracking linesearch
        negIdx1 = (dmu1 < 0)
        negIdx2 = (dmu2 < 0)
        step = 1
        if np.any(negIdx1):
            step = min(step, 0.99 * np.min(-mu1[negIdx1] / dmu1[negIdx1]))
        if np.any(negIdx2):
            step = min(step, 0.99 * np.min(-mu2[negIdx2] / dmu2[negIdx2]))

        for _ in range(MAXLSITER):
            newz = z + step * dz
            newmu1 = mu1 + step * dmu1
            newmu2 = mu2 + step * dmu2
            newf1 = newz - lambd
            newf2 = -newz - lambd

            newResDual = DDT @ newz - Dy + newmu1 - newmu2
            newResCent = np.concatenate([-newmu1 * newf1 - 1 / t, -newmu2 * newf2 - 1 / t])
            newResidual = np.concatenate([newResDual, newResCent])

            if (np.max([np.max(newf1), np.max(newf2)]) < 0 and
                    np.linalg.norm(newResidual) <= (1 - ALPHA * step) * np.linalg.norm(residual)):
                break
            step *= BETA

        # Update primal and dual variables
        z = newz
        mu1 = newmu1
        mu2 = newmu2
        f1 = newf1
        f2 = newf2

    # If the solution does not meet the stopping criterion
    x = y - D.T @ z
    status = 'maxiter exceeded' if iters >= MAXITER else 'solved'
    print(status)
    
    return x, z, status


if __name__ == '__main__':
    # Load data
    y = np.log(data[::-1, -1])  # reverse order, last column, log-scale
    dates = data[::-1, :3]      # reverse order, 1st to 3rd columns
    y = y.reshape((len(y), 1))
    lambd = 50

    x, z, status = l1tf(y, lambd)

    # Set index as date in dataframes
    df_dates = pd.DataFrame(dates).astype(int).astype(str)
    df_dates['-'] = '-'
    df_dates = df_dates[0] + df_dates['-'] + df_dates[1].apply(lambda x: x.zfill(2)) +  df_dates['-'] + df_dates[2].apply(lambda x: x.zfill(2))
    index = pd.to_datetime(df_dates, format='%Y-%m-%d')

    df_x = pd.DataFrame(x, index=index)
    df_y = pd.DataFrame(y, index=index)

    # Plot figure
    import matplotlib as mpl
    from matplotlib.ticker import (MultipleLocator, AutoMinorLocator, FuncFormatter, PercentFormatter)
    import matplotlib.dates as mdates


    rcparams = {
        'text.usetex': True,
        'font.family': 'sans-serif',
        'font.sans-serif': ['Times'],
        'axes.labelsize': 35,
        'axes.titlesize': 35,
        'legend.fontsize': 20,
        'ytick.right': 'off',
        'xtick.top': 'off',
        'ytick.left': 'on',
        'xtick.bottom': 'on',
        'xtick.labelsize': '25',
        'ytick.labelsize': '25',
        'axes.linewidth': 2.5,
        'xtick.major.width': 1.8,
        'xtick.minor.width': 1.8,
        'xtick.major.size': 14,
        'xtick.minor.size': 7,
        'xtick.major.pad': 10,
        'xtick.minor.pad': 10,
        'ytick.major.width': 1.8,
        'ytick.minor.width': 1.8,
        'ytick.major.size': 14,
        'ytick.minor.size': 7,
        'ytick.major.pad': 10,
        'ytick.minor.pad': 10,
        'axes.labelpad': 15,
        'axes.titlepad': 15,
        'axes.spines.right': False,
        'axes.spines.top': False}

    mpl.rcParams.update(rcparams)

    fig, ax = plt.subplots(figsize=(20, 10))
    ax.plot(df_x, color='b',lw=3)
    ax.plot(df_y, color='k', ls=':', lw=0.5)
    # ax.grid(axis='y', color='gray', ls='--', lw=0.5)
    ax.set_ylabel('log-price')
    ax.set_title('S\&P500: Log-Price and $\ell_1$-trend', loc='left')
    # ax.xaxis.set_major_formatter(mdates.DateFormatter("%b-%Y"))
    ax.xaxis.set_minor_locator(AutoMinorLocator(6))

    plt.show()